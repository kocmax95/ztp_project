import { BlogEntryCreateDto, BlogEntry, CreateBlogEntryModel } from "../Models/BlogEntry";
import { BlogCreateDtoValidation } from "../Validation/BlogEntryValidation";
import BlogEntryRepository from '../Repositories/BlogEntryRepository';
import { User } from "../Models/User";
import { NotFoundException, Exception } from '../Exceptions/ExceptionModel';
import UserService from "./UserService";

class BlogEntryService {

    private blogEntryRepository: BlogEntryRepository;
    private userService : UserService;

    constructor() {
        this.blogEntryRepository = new BlogEntryRepository();
        this.userService = new UserService();
    }
    
    Create = async (BlogEntryDto: BlogEntryCreateDto) => {
        BlogCreateDtoValidation(BlogEntryDto);

        const User : User = await this.userService.GetById(BlogEntryDto.Author);
        if(!User){
            const exception: Exception = NotFoundException(`User with Id ${BlogEntryDto.Author} not exists`);
            throw exception;
        }       

        const BlogEntryModel: BlogEntry = CreateBlogEntryModel(BlogEntryDto);
        const CreatedModel: BlogEntry = await this.blogEntryRepository.Save(BlogEntryModel);
        return CreatedModel;
    }

    GetAll = async (): Promise<Array<BlogEntry>> => {
        const BlogEntries: Array<BlogEntry> = await this.blogEntryRepository.GetAll();
        return BlogEntries;
    }

    GetById = async (Id: string) => {
        const BlogEntry : BlogEntry = await this.blogEntryRepository.GetById(Id);
        return BlogEntry;
    }
    
}

export default BlogEntryService;