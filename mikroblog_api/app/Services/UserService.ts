import { UserCreateDto, User, CreateUserModel, UserLoginData, UserResponseData, UserUpdateDto } from "../Models/User";
import UserRepository from "../Repositories/UserRepository";
import { ValidateUserCreateDto, ValidateUserLoginData, ValidateUserUpdateDto } from "../Validation/UserValidation";
import { Exception, NotFoundException, BadRequestException } from "../Exceptions/ExceptionModel";
import { UserModel } from '../Models/User';

class UserService {

    private userRepository: UserRepository;

    constructor() {
        this.userRepository = new UserRepository();
    }

    Create = async (user: UserCreateDto) => {
        ValidateUserCreateDto(user);

        let userFromDb = await this.userRepository.GetByLogin(user.Login);
        if (userFromDb) {
            const exception: Exception = BadRequestException('Ten login jest zajęty!');
            throw exception;
        }

        userFromDb = await this.userRepository.GetByMail(user.Mail);

        if (userFromDb) {
            const exception: Exception = BadRequestException('Ten adres mail jest zajęty!');
            throw exception;
        }

        const userModel = CreateUserModel(user);
        const CreatedUser = await this.userRepository.Save(userModel);
        return CreatedUser;
    }

    Login = async (data: UserLoginData) => {
        ValidateUserLoginData(data);

        let user = await this.userRepository.GetByLogin(data.Login);
        if (!user || user && user.Password !== data.Password) {
            const exception: Exception = BadRequestException('Login lub hasło jest nieprawidłowe!');

            throw exception;
        }

        user = new UserModel({
            _id: user._id,
            Login: user.Login,
            Mail: user.Mail,
        });

        const token = user.generateAuthToken();
        const responseData: UserResponseData = {
            Token: token,
            User: user
        }
        return responseData;
    }

    Update = async (data: UserUpdateDto): Promise<User> => {
        ValidateUserUpdateDto(data);

        const User: User = await this.userRepository.GetById(data._id);
        if (!User) {
            const exception: Exception = NotFoundException(`User with id: ${data._id} not exists!`);
            throw exception;
        }        

        const UpdatedUser: User = await this.userRepository.Update(data);
        return UpdatedUser;

    }

    GetById = async (Id: string) => {
        const User: User = await this.userRepository.GetById(Id);
        return User;
    }

}

export default UserService;