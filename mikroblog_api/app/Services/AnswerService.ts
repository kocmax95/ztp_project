import AnswerRepository from '../Repositories/AnswerRepository';
import { AnswerCreateDto, Answer, AnswerModel, CreateAnswerModel } from "../Models/Answer";
import { AnswerValidation, AnswerModelValidation } from '../Validation/AnswerValidation';
import { Exception, NotFoundException } from '../Exceptions/ExceptionModel';
import BlogEntryService from './BlogEntryService';
import UserService from './UserService';
import { User } from '../Models/User';
class AnswerService {

    private answerRepository: AnswerRepository;
    private blogEntryService: BlogEntryService;
    private userService: UserService;

    constructor() {
        this.answerRepository = new AnswerRepository();
        this.blogEntryService = new BlogEntryService();
        this.userService = new UserService();
    }

    Create = async (AnswerDto: AnswerCreateDto): Promise<Answer> => {
        AnswerValidation(AnswerDto);

        const BlogEntry: BlogEntry = await this.blogEntryService.GetById(AnswerDto.BlogEntry);

        if (!BlogEntry) {
            const exception: Exception = NotFoundException(`BlogEntry with Id ${AnswerDto.BlogEntry} not exists`);
            throw exception;
        }

        const User: User = await this.userService.GetById(AnswerDto.Author);

        if (!User) {
            const exception: Exception = NotFoundException(`User with Id ${AnswerDto.Author} not exists`);
            throw exception;
        }

        const AnswerModel: Answer = CreateAnswerModel(AnswerDto);
        const CreatedModel: Answer = await this.answerRepository.Save(AnswerModel);

        return CreatedModel;
    }

    GetByBlogEntry = async (BlogEntry: string): Promise<Array<Answer>> => {
        const Answers: Array<Answer> = await this.answerRepository.GetByBlogEntry(BlogEntry);
        return Answers;
    }

    Update = async (Answer: Answer): Promise<Answer> => {
        AnswerModelValidation(Answer);
        const AnswerFromDb = await this.answerRepository.GetById(Answer._id);

        if (!AnswerFromDb) {
            const exception: Exception = NotFoundException(`Answer with Id ${Answer._id} not exists`);
            throw exception;
        }

        const UpdatedAnswer = await this.answerRepository.Update(Answer);

        return UpdatedAnswer;
    }

    Delete = async (Id: string) => {
        const AnswerFromDb = await this.answerRepository.GetById(Id);

        if (!AnswerFromDb) {
            const exception: Exception = NotFoundException(`Answer with Id ${Id} not exists`);
            throw exception;
        }

        await this.answerRepository.Delete(AnswerFromDb);
    }

}

export default AnswerService;
