import { BlogEntryModel, BlogEntry } from '../Models/BlogEntry';

class BlogEntryRepository {

    Save = async (BlogEntry: BlogEntry) => {
        return await BlogEntryModel(BlogEntry).save();
    }

    GetAll = async () => {
        return await BlogEntryModel.find();
    }

    GetById = async (Id: string) => {
        return await BlogEntryModel.findById(Id);
    }

}

export default BlogEntryRepository;