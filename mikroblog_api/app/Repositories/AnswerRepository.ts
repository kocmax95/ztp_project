import { AnswerModel, Answer } from '../Models/Answer';

class AnswerRepository {

    Save = async (Answer: Answer) => {
        return await AnswerModel(Answer).save();
    }

    GetByBlogEntry = async (BlogEntry: string) => {
        const answers = await AnswerModel.find({
            BlogEntry: BlogEntry
        });
        return answers;
    }

    GetById = async (Id: string) => {
        const Answer = await AnswerModel.findById(Id);
        return Answer;
    }

    Update = async (Answer: Answer) => {
        await AnswerModel.findByIdAndUpdate({ _id: Answer._id }, Answer);
        const UpdatedAnswer = AnswerModel.findById(Answer._id);

        return UpdatedAnswer;
    }

    Delete = async (Answer: Answer) => {
        await AnswerModel.remove(Answer);
    }

}

export default AnswerRepository;