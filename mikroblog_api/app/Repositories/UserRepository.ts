import { UserModel, User, UserUpdateDto } from '../Models/User';

class UserRepository {

    async Save(User: User) {
        return await UserModel(User).save();
    }

    GetByLogin = async (Login: string) => {
        return await UserModel.findOne({
            Login: Login
        });
    }

    GetByMail = async (Mail: string) => {
        return await UserModel.findOne({
            Mail: Mail
        });
    }

    GetById = async (Id: string) => {
        return await UserModel.findById(Id);
    }

    Update = async (User: UserUpdateDto) => {
        await UserModel.findByIdAndUpdate({ _id: User._id }, User);
        const UpdatedUser = await this.GetById(User._id);
        return UpdatedUser;
    }


}

export default UserRepository;