import { Exception, BadRequestException } from '../Exceptions/ExceptionModel';
import * as moment from 'moment';

export const lengthValidation = (value: string, name: string, minLength: number, maxLength: number): void => {

    if (value.length < minLength) {
        const exception: Exception = BadRequestException(`${name} should contains at least ${minLength} characters !`);
        throw exception;
    }

    if (value.length > maxLength) {
        const exception: Exception = BadRequestException(`${name} should contains maximum ${maxLength} characters !`);
        throw exception;
    }
}

export const exactLengthValidation = (value: string, name: string, length: number) => {
    if (value.length != length) {
        const exception: Exception = BadRequestException(`${name} should contains exactly ${length} characters!`)
        throw exception;
    }
}

export const stringValidation = (value: string, name: string): void => {

    const regexp: RegExp = new RegExp(/^[a-zA-Z]*$/g);
    if (!regexp.test(value)) {
        const exception: Exception = BadRequestException(`${name} should contains only characters !`);
        throw exception;
    }

}

export const guidValidation = (guid: string, name: string) => {
    const regExp = new RegExp(/^[0-9a-f]{8}-[0-9a-f]{4}-[1-5][0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}$/i);
    if (!regExp.test(guid)) {
        const exception: Exception = BadRequestException(`${name} is invalid !`);
        throw exception;
    }
}

export const dateValidation = (date: string, name: string) => {
    if (!moment(date).isValid()) {
        const exception: Exception = BadRequestException(`${name} is invalid !`);
        throw exception;
    }
}

export const mailValidation = (mail: string) => {
    const regExp: RegExp = new RegExp(/^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/i);
    if (!regExp.test(mail)) {
        const exception: Exception = BadRequestException('Mail is invalid !');
        throw exception;
    }
}

