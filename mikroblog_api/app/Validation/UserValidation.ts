import { UserCreateDto, UserLoginData, UserUpdateDto } from '../Models/User';
import { Exception, BadRequestException, CustomException } from '../Exceptions/ExceptionModel';
import { lengthValidation, mailValidation, stringValidation } from './BaseValidation';

export const ValidateUserCreateDto = (user: UserCreateDto) => {

    if (!user) {
        const exception: Exception = BadRequestException('User model is null !');
        throw exception;
    }

    if (!user.Login) {
        const exception: Exception = BadRequestException('Login is required !');
        throw exception;
    }
    lengthValidation(user.Login, 'Login', 3, 50);
    
    if (!user.Password) {
        const exception: Exception = BadRequestException('Password is required !');
        throw exception;
    }
    lengthValidation(user.Password, 'Password', 3, 50);

    if (!user.Mail) {
        const exception: Exception = BadRequestException('Mail is required !');
        throw exception;
    }
    lengthValidation(user.Mail, 'Mail', 3, 50);
    mailValidation(user.Mail);

}

export const ValidateUserLoginData = (data: UserLoginData) => {

    if (!data.Login) {
        const exception: Exception = BadRequestException('Login is required !');
        throw exception;
    }

    if (!data.Password) {
        const exception: Exception = BadRequestException('Password is required !');
        throw exception;
    }
}

export const ValidateUserUpdateDto = (data: UserUpdateDto) => {

    if(!data._id){
        const exception: Exception = BadRequestException('Id is required!');
        throw exception;
    }

    if (data.FirstName) {
        lengthValidation(data.FirstName,'FirstName',3,50);
        stringValidation(data.FirstName,'FirstName');
    }

    if (data.LastName) {
        lengthValidation(data.LastName,'LastName',3,50);
        stringValidation(data.LastName,'LastName');
    }       

}