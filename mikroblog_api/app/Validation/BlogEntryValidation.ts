import { BlogEntryCreateDto } from "../Models/BlogEntry";
import { Exception, BadRequestException } from '../Exceptions/ExceptionModel';
import { lengthValidation, exactLengthValidation } from "./BaseValidation";


export const BlogCreateDtoValidation = (dto: BlogEntryCreateDto) => {

    if (!dto) {
        const exception: Exception = BadRequestException('BlogEntryModel is null!');
        throw exception;
    }

    if (!dto.Title) {
        const exception: Exception = BadRequestException('Title is required!');
        throw exception;
    }
    lengthValidation(dto.Title, 'Title', 3, 200);

    if (!dto.Content) {
        const exception: Exception = BadRequestException('Content is required!');
        throw exception;
    }
    lengthValidation(dto.Content, 'Title', 3, 1000);

    if (!dto.Author) {
        const exception: Exception = BadRequestException('Author is required!');
        throw exception;
    }

}