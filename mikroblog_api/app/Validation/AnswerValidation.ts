import { AnswerCreateDto, Answer } from "../Models/Answer";
import { Exception, BadRequestException } from '../Exceptions/ExceptionModel';
import { lengthValidation, dateValidation, exactLengthValidation } from "./BaseValidation";

export const AnswerValidation = (dto: AnswerCreateDto) => {
    if (!dto) {
        const exception: Exception = BadRequestException("BlogEntryModel is null!");
        throw exception;
    }

    if (!dto.Content) {
        const exception: Exception = BadRequestException("Content is required!");
        throw exception;
    }
    lengthValidation(dto.Content,"Title",3,1000);

    if (!dto.Author) {
        const exception: Exception = BadRequestException("Author is required!");
        throw exception;
    }

    if (!dto.BlogEntry) {
        const exception: Exception = BadRequestException("BlogEntry is required!");
        throw exception;
    }
    exactLengthValidation(dto.BlogEntry,'BlogEntry',24);
}

export const AnswerModelValidation = (model: Answer) => {

    const AnswerDto: AnswerCreateDto = {
        Content: model.Content,
        Author: model.Author,
        BlogEntry: model.BlogEntry
    }
    AnswerValidation(AnswerDto);

    if (!model.CreationDate) {
        const exception: Exception = BadRequestException("CreationDate is required!");
        throw exception;
    }
    dateValidation(model.CreationDate,'CreationDate')

    if (!model.Evaluation) {
        const exception: Exception = BadRequestException("Evaluation is required!");
        throw exception;
    }

    if(model.Evaluation.Minuses < 0){
        const exception: Exception = BadRequestException("Minuses must be  zero or positive value!");
        throw exception;
    }

    if(model.Evaluation.Pluses < 0){
        const exception: Exception = BadRequestException("Pluses must be  zero or positive value!");
        throw exception;
    }

    if(model.Evaluation.Summary < 0){
        const exception: Exception = BadRequestException("Summary must be  zero or positive value!");
        throw exception;
    }

}