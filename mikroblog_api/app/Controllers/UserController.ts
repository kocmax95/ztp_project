import express = require('express');
import { exceptionHandler } from '../Helpers/ExceptionHandler';
import UserService from '../Services/UserService';
import { Auth } from '../Middlewares/AuthenticationMiddleware';
import { SendOk } from '../Helpers/ResponseHelper';
import { User } from '../Models/User';

const UserController = (app: express.Application) => {

    const userService: UserService = new UserService();

    app.post('/api/user', async (request: express.Request, response: express.Response) => {
        try {
            console.log(request.body);
            const user = await userService.Create(request.body);
            console.log(user);
            response.send(user);
        } catch (error) {
            exceptionHandler(error, response);
        }
    });

    app.post('/api/user/login', async (request: express.Request, response: express.Response) => {
        try {
            const userData = await userService.Login(request.body);
            console.log('userData: ',userData);
            
            response.header("x-auth-token", userData.Token);
            SendOk({
                _id: userData.User._id,
                Login: userData.User.Login,
                Mail: userData.User.Mail
            }, response);

        } catch (error) {
            exceptionHandler(error, response);
        }
    });

    app.put('/api/user', Auth, async (request: express.Request, response: express.Response) => {
        try {
            const UpdatedUser: User = await userService.Update(request.body);
            SendOk(UpdatedUser, response);
        } catch (error) {
            exceptionHandler(error, response);
        }
    });

    app.get('/api/user/:userId', Auth, async (request: express.Request, response: express.Response) => {
        try {
            const UserData: User = await userService.GetById(request.params.userId);
            SendOk(UserData, response);
        } catch (error) {
            exceptionHandler(error, response);
        }
    });

}

export default UserController;