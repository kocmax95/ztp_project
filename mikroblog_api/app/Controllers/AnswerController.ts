import AnswerService from '../Services/AnswerService';
import { Auth } from '../Middlewares/AuthenticationMiddleware';
import express = require('express');
import { exceptionHandler } from '../Helpers/ExceptionHandler';
import { SendOk } from '../Helpers/ResponseHelper';

const AnswerController = (app: express.Application) => {
    const answerService = new AnswerService();

    app.post('/api/answer', Auth, async (request: express.Request, response: express.Response) => {

        try {
            const Answer = await answerService.Create(request.body);
            SendOk(Answer, response);

        } catch (error) {
            exceptionHandler(error, response);
        }

    })

    app.get('/api/answer/:blogEntryId', async (request: express.Request, response: express.Response) => {
        try {
            const Answers = await answerService.GetByBlogEntry(request.params.blogEntryId);
            SendOk(Answers, response);
        } catch (error) {
            exceptionHandler(error, response);
        }
    })

    app.put('/api/answer', Auth, async (request: express.Request, response: express.Response) => {
        try {
            const Answer = await answerService.Update(request.body);
            SendOk(Answer, response);
        } catch (error) {
            exceptionHandler(error, response);
        }
    })

    app.delete('/api/answer/:answerId', Auth, async (request: express.Request, response: express.Response) => {
        try {
            await answerService.Delete(request.params.answerId);
            SendOk("Pomyślnie usunięto", response);
        } catch (error) {
            exceptionHandler(error, response);
        }
    })
}

export default AnswerController;