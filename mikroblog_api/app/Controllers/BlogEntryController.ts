import BlogEntryService from '../Services/BlogEntryService';
import { Auth } from '../Middlewares/AuthenticationMiddleware';
import express = require('express');
import { exceptionHandler } from '../Helpers/ExceptionHandler';
import { BlogEntry } from '../Models/BlogEntry';
import { SendOk } from '../Helpers/ResponseHelper';


const BlogEntryController = (app: express.Application) => {

    const blogEntryService: BlogEntryService = new BlogEntryService();

    app.post('/api/blogEntry', Auth, async (request: express.Request, response: express.Response) => {
        try {
            console.log('dane: ',request.body);
            
            const BlogEntry = await blogEntryService.Create(request.body);
            response.send(BlogEntry);
        } catch (error) {
            exceptionHandler(error, response);
        }
    })

    app.get('/api/blogEntry', Auth, async (request: express.Request, response: express.Response) => {
        try {
            const BlogEntries: Array<BlogEntry> = await blogEntryService.GetAll();
            SendOk(BlogEntries, response);
        } catch (error) {
            exceptionHandler(error, response);
        }
    });

}

export default BlogEntryController;