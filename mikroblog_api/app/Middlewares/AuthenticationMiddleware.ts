import jwt = require("jsonwebtoken");
import { myPrivateKey } from '../appsettings';
import express = require('express');
import { SendWithStatusCode } from "../Helpers/ResponseHelper";

export const Auth = (request: express.Request, response: express.Response, next: express.NextFunction) => {

  const token: string | string[] = request.headers["x-auth-token"] || request.headers["authorization"];
  console.log("token: ",token);
  

  if (!token) {
    SendWithStatusCode(401, "Access denied. No token provided.", response);
    return;
  }

  try {
    const decoded = jwt.verify(token, myPrivateKey);
    request.user = decoded;
    next();
  } catch (ex) {
    SendWithStatusCode(401, "Invalid token.", response);
  }
};