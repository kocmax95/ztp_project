import * as moment from 'moment';
import { Evaluation } from './BlogEntry';
const mongoose = require('mongoose');
const { Schema } = mongoose;


const AnswerSchema = new Schema({
    Content: String,
    Author: String,
    BlogEntry: String,
    CreationDate: String,
    Evaluation: {
        Pluses: Number,
        Minuses: Number,
        Summary: Number
    }
});

export interface AnswerCreateDto {
    Content: string,
    Author: string,
    BlogEntry: string
}

export interface Answer {
    _id?: string,
    Content: string,
    Author: string,
    BlogEntry: string,
    CreationDate: string,
    Evaluation: Evaluation
}

export const CreateAnswerModel = (blogEntryCreateDto: AnswerCreateDto) => {
    const CreationDate = moment().format('YYYY-MM-DD HH:MM:SS');
    const Evaluation: Evaluation = {
        Pluses: 0,
        Minuses: 0,
        Summary: 0
    }
    return {
        Content: blogEntryCreateDto.Content,
        Author: blogEntryCreateDto.Author,
        BlogEntry: blogEntryCreateDto.BlogEntry,
        CreationDate: CreationDate,
        Evaluation: Evaluation
    }
}

export const AnswerModel = mongoose.model('Answers', AnswerSchema, 'answers');
