import * as moment from 'moment';
const mongoose = require('mongoose');
const { Schema } = mongoose;

const BlogEntrySchema = new Schema({
    Title: String,
    Content: String,
    Author: String,
    CreationDate: String,
    Evaluation: {
        Pluses: Number,
        Minuses: Number,
        Summary: Number
    }
});

export interface Evaluation {
    Pluses: Number,
    Minuses: Number,
    Summary: Number
}

export interface BlogEntryCreateDto {
    Title: string,
    Content: string,
    Author: string,
}

export interface BlogEntry {
    _id?: string,
    Title: string,
    Content: string,
    Author: string,
    CreationDate: String,
    Evaluation: Evaluation
}

export const CreateBlogEntryModel = (blogEntryCreateDto: BlogEntryCreateDto) => {
    const CreationDate = moment().format('YYYY-MM-DD HH:MM:SS');
    const Evaluation: Evaluation = {
        Pluses: 0,
        Minuses: 0,
        Summary: 0
    }

    return {
        Title: blogEntryCreateDto.Title,
        Content: blogEntryCreateDto.Content,
        Author: blogEntryCreateDto.Author,
        CreationDate: CreationDate,
        Evaluation: Evaluation
    }
}

export const BlogEntryModel = mongoose.model('BlogEntries', BlogEntrySchema, 'blogEntries');