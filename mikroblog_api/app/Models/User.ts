const mongoose = require('mongoose');
const { Schema } = mongoose;
const jwt = require('jsonwebtoken');
const Joi = require('joi');
import { myPrivateKey } from '../appsettings';

export interface UserCreateDto {
    Login: string,
    Password: string,
    Mail: string
}

export interface User {
    _id?: string,
    FirstName?: string,
    LastName?: string,
    Login: string,
    Password: string,
    Mail: string,
    VerificationCode?: string,
    City?: string,
    Age?: string

}

export interface UserUpdateDto {
    _id: string,
    FirstName?: string,
    LastName?: string,
    City?: string,
    Age?: string
}

export interface UserLoginData {
    Login: string,
    Password: string
}

const UserSchema = new Schema({
    FirstName: String,
    LastName: String,
    Login: String,
    Password: String,
    Mail: String,
    VerificationCode: String,
    IsAdmin: {
        type: Boolean,
        default: false
    },
    City: String,
    Age: String
});

export interface UserResponseData {
    Token: string,
    User: {
        _id?: string,
        Login: string,
        Mail: string
    }
}

UserSchema.methods.generateAuthToken = function () {
    const token = jwt.sign({ _id: this._id, isAdmin: this.isAdmin }, myPrivateKey);
    return token;
}

export const validateUser = (user) => {
    const schema = {
        Login: Joi.string().min(3).max(50).required(),
        Password: Joi.string().min(3).max(255).required()
    };

    return Joi.validate(user, schema);
}

export const CreateUserModel = (user: UserCreateDto): User => {
    return {
        Login: user.Login,
        Password: user.Password,
        Mail: user.Mail
    }
}

export const UserModel = mongoose.model('users', UserSchema, 'users');