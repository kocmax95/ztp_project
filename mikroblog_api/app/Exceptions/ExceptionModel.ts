export interface Exception {
    code: number,
    message: string
}

export const BadRequestException = (message) => {
    return BaseException(message, 400);
}

export const NotFoundException = (message) => {
    return BaseException(message, 404);
}

const BaseException = (message, code) => {
    const exception: Exception = {
        code: code,
        message: message
    }
    return exception;
}


export class CustomException extends Error {
    public code : number;

    constructor(code: number, message: string) {
        super(message);
        this.code = code;
    }
}