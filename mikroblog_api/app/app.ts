import express = require('express');
import { hostSettings, getConnectionString } from './appsettings';
import UserController from './Controllers/UserController';
import BlogEntryController from './Controllers/BlogEntryController';
import AnswerController from './Controllers/AnswerController';
import mongoose = require('mongoose');
import bodyParser = require('body-parser');
import cors  = require('cors');
const app: express.Application = express();

mongoose.connect('mongodb://localhost:27017/Mikroblog', {
    useNewUrlParser: true,
    useCreateIndex: true
}, (err) => {
    if (!err) {
        console.log('Connection with database established...');
    } else {
        console.log(err);
    }
});

const options:cors.CorsOptions = {
    allowedHeaders: ["Origin", "X-Requested-With", "Content-Type", "Accept", "X-Access-Token","x-auth-token"],
    credentials: true,
    methods: "GET,HEAD,OPTIONS,PUT,PATCH,POST,DELETE",
    origin: "http://localhost:3000",
    preflightContinue: false,
    exposedHeaders: "x-auth-token"
  };
  
app.use(cors(options));

app.use(bodyParser.json());

UserController(app);
BlogEntryController(app);
AnswerController(app);

app.options("*", cors(options));

app.listen(hostSettings.port, function () {
    console.log(`App listening on port ${hostSettings.port}!`);
});
