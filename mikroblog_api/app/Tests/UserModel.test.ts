import { CreateUserModel, UserCreateDto, User } from '../Models/User';

test('Should create correct UserModel', () => {

    // Arrange
    const user : UserCreateDto = {
        Login: 'kuba481',
        Password: 'Impala',
        Mail: 'kuba@onet.eu'
    }

    // Act
    const userModel: User = CreateUserModel(user);

    // Expect
    expect(userModel.Login).toBe(user.Login);
    expect(userModel.Password).toBe(user.Password);
    expect(userModel.Mail).toBe(user.Mail);
});