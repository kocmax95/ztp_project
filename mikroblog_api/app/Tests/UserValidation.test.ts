import { ValidateUserCreateDto } from '../Validation/UserValidation';
import { UserCreateDto } from '../Models/User';
import { CustomException, Exception } from '../Exceptions/ExceptionModel';

test('Should validate UserCreateDto correctly', () => {

    // Arrange
    const UserCreateDto = {
        Login: "kuba481",
        Password: 'Impala',
        Mail: "kuba@onet.eu"
    };

    // Act
    const result = ValidateUserCreateDto(UserCreateDto);

    // Expect
    expect(result).toBeUndefined();
});

test('Should throw exception when login is null', () => {

    // Arrange
    const UserCreateDto = {
        Login: null,
        Password: 'Impala',
        Mail: "kuba@onet.eu"
    };
    let exception : Exception = null;

    // Act
    try {
        ValidateUserCreateDto(UserCreateDto);
    } catch (error) {
        exception = error;
    };

    // Expect
    expect(exception.code).toBe(400);
    expect(exception.message).toBe('Login is required !');
});

test('Should throw exception when password is null', () => {

    // Arrange
    const UserCreateDto = {
        Login: 'kuba481',
        Password: null,
        Mail: "kuba@onet.eu"
    };
    let exception : Exception = null;

    // Act
    try {
        ValidateUserCreateDto(UserCreateDto);
    } catch (error) {
        exception = error;
    };

    // Expect
    expect(exception.code).toBe(400);
    expect(exception.message).toBe('Password is required !');
});

test('Should throw exception when mail is null', () => {

    // Arrange
    const UserCreateDto = {
        Login: 'kuba481',
        Password: 'Impala',
        Mail: null
    };
    let exception : Exception = null;

    // Act
    try {
        ValidateUserCreateDto(UserCreateDto);
    } catch (error) {
        exception = error;
    };

    // Expect
    expect(exception.code).toBe(400);
    expect(exception.message).toBe('Mail is required !');
});


test('Should throw exception when mail is incorrect', () => {

    // Arrange
    const UserCreateDto = {
        Login: 'kuba481',
        Password: 'Impala',
        Mail: 'dsfasdf'
    };
    let exception : Exception = null;

    // Act
    try {
        ValidateUserCreateDto(UserCreateDto);
    } catch (error) {
        exception = error;
    };

    // Expect
    expect(exception.code).toBe(400);
    expect(exception.message).toBe('Mail is invalid !');
});
