import { exactLengthValidation, lengthValidation, stringValidation, dateValidation } from '../Validation/BaseValidation';
import { Exception } from '../Exceptions/ExceptionModel';
import * as moment from 'moment';

test('Should throw exception when lenght is not equal', () => {

    // Arrange
    const login = 'kuba481';
    let expectedError = null;

    // Act
    try {
        exactLengthValidation(login, 'Login', 15);
    } catch (error) {
        expectedError = error;
    }

    // Expect
    expect(expectedError.code).toBe(400);
    expect(expectedError.message).toBe('Login should contains exactly 15 characters!');

});

test('Should pass validation when length is ok', ()=> {
    // Assert
    const login = 'kuba481';

    // Act and expect
    expect(exactLengthValidation(login,'Login',7)).toBeUndefined();
    
})

test('Should throw exception when lenght is not in range', () => {

    // Arrange
    const login = 'ku';
    let expectedError = null;

    //Act
    try {
        lengthValidation(login, 'Login', 3, 50);
    } catch (error) {
        expectedError = error;
    }

    // Expect
    expect(expectedError.code).toBe(400);
    expect(expectedError.message).toBe('Login should contains at least 3 characters !');

});


test('Should throw exception when value is not plain string', () => {

    // Arrange
    const value = 'ku323f32';
    let expectedError = null;

    //Act
    try {
        stringValidation(value, 'Title');
    } catch (error) {
        expectedError = error;
    }

    // Expect
    expect(expectedError.code).toBe(400);
    expect(expectedError.message).toBe('Title should contains only characters !');

});

test('Should pass validation when string is correct', () => {
    // Arrange
    const value = 'adsfasdf';
    // Act and expect
    expect(stringValidation(value, 'Title')).toBeUndefined();
});

test('Should throw exception when date is incorrect',()=> {

    // Arrange
    const badDate = '2019-04-ee';
    let expectedError: Exception  = null;

    // Act
    try {
        dateValidation(badDate,'Date');
    } catch (error) {
        expectedError = error;
    }

    // Expect
    expect(expectedError.code).toBe(400);
    expect(expectedError.message).toBe('Date is invalid !')

});

test('Should pass validation when date is correct',()=>{

    // Assert
    const correctDate = moment().format('YYYY-MM-DD');

    // Act and expect
    expect(dateValidation(correctDate,'Date')).toBeUndefined();

});