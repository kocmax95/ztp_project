import {CreateAnswerModel, AnswerCreateDto} from '../Models/Answer';

test('Should create correct AnswerModel',()=>{
     // Arrange
     const answer : AnswerCreateDto = {
         Content: 'Treść notatki',
         Author: 'ha7vh7ahvsg9ghae87gh',
         BlogEntry: 'afhieufhaifoe4igfesg5'
     }

     // Act
     const answerModel = CreateAnswerModel(answer);

     // Expect
     expect(answerModel.Content).toBe(answer.Content);
     expect(answerModel.Author).toBe(answer.Author);
     expect(answerModel.BlogEntry).toBe(answer.BlogEntry);
     expect(answerModel.Evaluation.Minuses).toBe(0);
     expect(answerModel.Evaluation.Pluses).toBe(0);
     expect(answerModel.Evaluation.Summary).toBe(0);
});