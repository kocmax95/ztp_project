export const GetRequestQueryParameters = (requestUrl: string): any => {
    const parametersObject = {};
    const queryString = requestUrl.split('?')[1];
    const queryParameters = queryString.split('&');

    queryParameters.forEach(parameterPair => {
        const replacedParameter = parameterPair.replace('%20', ' ');
        const parameterName = replacedParameter.split('=')[0];
        const parameterValue = replacedParameter.split('=')[1];

        parametersObject[parameterName] = parameterValue;
    });

    return parametersObject;
} 