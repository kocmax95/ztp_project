export const SendWithStatusCode = (code, message, response) => {
    response.status(code);
    response.send({
        message: message
    });
}

export const SendOk = (message: any, response: any) => {
    response.status(200);
    response.send(message);
}