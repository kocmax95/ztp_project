import { SendWithStatusCode } from './ResponseHelper';

export const exceptionHandler = (error, response) => {
    const { code, message } = error;
    console.log('Error: ', error);
    if (code) {
        SendWithStatusCode(code, message, response);
    }
    else {
        SendWithStatusCode(500, "Wystąpił błąd, skontaktuj się z administratorem", response);
    }
}