import React from 'react';
import {
  BrowserRouter as Router,
  // Switch,
  Route,
  // Link
} from "react-router-dom";

import './App.css';
import Register from './Components/Register';
import Header from './Components/Header';
import Login from './Components/Login';
import AddPost from './Components/AddPost';
import EditProfile from './Components/EditProfile'
import Profile from './Components/Profile'
import Card from './Components/Card'
import HeaderAfterLogin from './Components/HeaderAfterLogin';
// import { render } from '@testing-library/react';


class App extends React.Component {
  state = {

  }

  // if(userLogged === null){
  //   return <Header/>
  // }
  // else{
  //   return <HeaderAfterLogin/>;
  // }


render(){
  const userLogged = sessionStorage.getItem("userId");

  return (
    <Router>

      {userLogged === null ? <Header /> : <HeaderAfterLogin />}
      <Route exact path='/' component={Card} />
      <Route path='/login' component={Login} />
      <Route path='/register' component={Register} />
      <Route path='/profile' component={Profile} />
      <Route path='/editProfile' component={EditProfile} />
      <Route path='/addPost' component={AddPost} />

    </Router>
  );
}
}

export default App;
