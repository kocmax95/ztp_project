import axios from 'axios'

export const login = async (loginData) => {

    let result = await axios.post('http://localhost:8080/api/user/login', loginData);

    console.log('result in api: ',result);
    
    if(result.status === 200){
        result = {
            userData : result.data,
            accesToken : result.headers['x-auth-token']
        }
    } else {console.log(`jakiś błąd`);}

    return result;
}

export const register = async (registerData) => {

    let result = await axios.post('http://localhost:8080/api/user', registerData);

    return result;
}

export const editProfile = async (editProfileData) => {

    const accesToken = sessionStorage.getItem('accesToken');
    let result = await axios.put('http://localhost:8080/api/user', editProfileData,{
        headers: {
            'x-auth-token' : accesToken
        }
    });
    console.log(result);
    return result;
}

export const getById = async (id) => {

    const accesToken = sessionStorage.getItem('accesToken');
    let result = await axios.get(`http://localhost:8080/api/user/${id}`,{
        headers: {
            'x-auth-token' : accesToken
        }
    });
    return result.data;
}



