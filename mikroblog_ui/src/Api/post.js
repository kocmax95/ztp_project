import axios from 'axios'

export const addPost = async (addPostData) => {
    const accesToken = sessionStorage.getItem('accesToken');

    let result = await axios.post('http://localhost:8080/api/blogEntry', addPostData,{
        headers: {
            'x-auth-token' : accesToken
        }
    })

    return result;
}

export const getPosts = async () => {
    const accesToken = sessionStorage.getItem('accesToken')
    let result = await axios.get('http://localhost:8080/api/blogEntry', {
        headers: {
            'x-auth-token' : accesToken
        }
    });

    console.log(`result in api:`, result);

    if(result.status === 200){
        result = {
            postsData: result.data,
            accesToken: result.headers['x-auth-token']
        } 
    }else {
            console.log(`Jakiś błąd`);
    }
    console.log(result);
    return result
}