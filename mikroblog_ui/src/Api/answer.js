import axios from 'axios'

export const answear = async (answerData) => {
    const accesToken = sessionStorage.getItem('accesToken');

    let result = await axios.post('http://localhost:8080/api/answear',answerData,{
        headers: {
            'x-auth-token' : accesToken
        }
    })

    return result;
}