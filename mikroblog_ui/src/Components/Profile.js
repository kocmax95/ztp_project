import React from 'react';
import {Link} from 'react-router-dom'
import 'bootstrap/dist/css/bootstrap.css'
import { getById } from '../Api/user'
class Profile extends React.Component {
    state={
        name: "",
        surname: "",
        city: "",
        age: "",
    }

    componentDidMount = async () =>{
        const userId = this.GetUserId();
        const userData = await getById(userId);
        console.log('userData:',userData);
        
        this.setState({
            name: userData.FirstName,
            surname: userData.LastName,
            city: userData.City,
            age: userData.Age,
        })
    }
    GetUserId = () => {
        return sessionStorage.getItem('userId');
    }
    render() {

        const state = this.state;
        return (
            <>
                <div className="container">
                    <div className="card col-4 mx-auto mt-5">
                   
                        <div className="card-body">

                            <div className="d-flex justify-content-between">
                                <div className="font-weight-bold">Imię:</div>
                                <div className="">{state.name}</div>
                            </div>
                            <div className="d-flex justify-content-between">
                                <div className="font-weight-bold">Nazwisko:</div>
                                <div className="">{state.surname}</div>
                            </div>
                            <div className="d-flex justify-content-between">
                                <div className="font-weight-bold">Miejscowość:</div>
                                <div className="">{state.city}</div>
                            </div>
                            <div className="d-flex justify-content-between">
                                <div className="font-weight-bold">Wiek:</div>
                                <div className=""><span>{state.age}</span> lat</div>
                            </div>

                            <Link to='/editProfile'><div className="btn btn-danger mt-3">Edytuj profil</div></Link>

                        
                        </div>

                    </div>
                </div>
            </>
        )
    }

}

export default Profile;





