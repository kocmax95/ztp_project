import React from 'react';
import 'bootstrap/dist/css/bootstrap.css'
import { register } from '../Api/user'

class Register extends React.Component {
    state = {
        login: '',
        mail: '',
        password: '',

    }

    // handleInputChange = (e) => {
    //     this.setState({
    //         [e.target.name]: e.target.value
    //     }, ()=> {
    //         console.log(this.state);
    //     })
    // }
    handleInputChangeNick = (e) => {
        this.setState({
          login: e.target.value,
        }, ()=> {
            // console.log(this.state.nick)
        })
    }
    handleInputChangeEmail = (e) => {
        this.setState({
          mail: e.target.value,
        }, ()=>{
            // console.log(this.state.email)
        })

    }
    handleInputChangePassword = (e) => {
        this.setState({
          password: e.target.value,
        })
    }

    handleSubmitRegister = async (e) => {
        e.preventDefault();
        console.log(`Podano następujące dane: nick: ${this.state.login}, email: ${this.state.mail}, password: ${this.state.password}`);
        const registerData = {
            Login: this.state.login,
            Mail: this.state.mail,
            Password: this.state.password
        }

        const result = await register(registerData);
        console.log(result);
    }

    render() {
        return (
            <>
                <div className="container card bg-light p-5 mt-5 col-3">
                    <form>
                        <h3>Rejestracja</h3>
                        <div className="form-group">
                            <label htmlFor="registerInputNick">Podaj Nick:</label>
                            <input type="text" name="nick" value={this.state.nick} onChange={this.handleInputChangeNick} className="form-control" id="registerInputNick" />
                        </div>
                        <div className="form-group">
                            <label htmlFor="registerInputEmail">Email address:</label>
                            <input type="email" name="email" value={this.state.email} onChange={this.handleInputChangeEmail} className="form-control" id="registerInputEmail" />
                        </div>
                        <div className="form-group">
                            <label htmlFor="registerInputpassword">Password:</label>
                            <input type="password" name="password" value={this.state.password} onChange={this.handleInputChangePassword} className="form-control" id="registerInputpassword" />
                        </div>
                        <button type="submit" onClick={this.handleSubmitRegister} className="btn btn-primary">Zarejestruj</button>
                    </form>
                </div>
            </>
        )
    }

}

export default Register;
