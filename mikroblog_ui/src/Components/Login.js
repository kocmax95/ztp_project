import React from 'react';
import 'bootstrap/dist/css/bootstrap.css'
import { login } from '../Api/user'

class Login extends React.Component {
    state = {
        email: '',
        password: '',
        result: '',
    }

    handleInputChangeEmail = (e) => {
        this.setState({
            email: e.target.value,
        })
        

    }
    handleInputChangePassword = (e) => {
        this.setState({
            password: e.target.value,
        })
    }

    handleSubmitLogin = async (e) => {
        e.preventDefault()
        console.log(`Podano nastepujące dane: email:${this.state.email}, password:${this.state.password}`)
        const loginData = {
            Login: this.state.email,
            Password: this.state.password
        }

        const result = await login(loginData);
        console.log(result);

        this.setState({
            result: result.userData
        })

        console.log('result: ',result);
        
        
        this.SetSessionStorage(result);

    }

    SetSessionStorage = (result) => {
        sessionStorage.setItem('userId', result.userData._id);
        sessionStorage.setItem('accesToken',result.accesToken);
    }

    render() {
        return (
            <>
                <div className="container card bg-light p-5 mt-5 col-3">
                    <form className="">
                        <h3>Logowanie</h3>
                        {/* <h3>{this.state.result}</h3> */}
                        <div className="form-group">
                            <label htmlFor="loginEmail">Email address:</label>
                            <input type="email" onChange={this.handleInputChangeEmail} value={this.state.email} className="form-control" id="loginEmail" />
                        </div>

                        <div className="form-group">
                            <label htmlFor="loginPassword">Password:</label>
                            <input type="password" onChange={this.handleInputChangePassword} value={this.state.password} className="form-control" id="loginPassword" />
                        </div>

                        <button type="submit" onClick={this.handleSubmitLogin} className="btn btn-primary">Zaloguj</button>
                    </form>
                </div>
            </>
        )
    }

}

export default Login;
