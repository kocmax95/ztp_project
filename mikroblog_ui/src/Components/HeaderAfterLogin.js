import React from 'react';
import { Link } from 'react-router-dom'
import 'bootstrap/dist/css/bootstrap.css'

class HeaderAfterLogin extends React.Component {
    state = {}

    handleBtnLogOut = (e) => {
        sessionStorage.setItem('userId',null)
        sessionStorage.setItem('accesToken',null)
    }
    render() {
        return (
            <>
                <div className="container d-flex justify-content-between ">
                    <Link to='/'><h1 className="display-4">Microblog</h1></Link>
                    <div>
                        <Link to='/addPost'><button className="btn btn-light justivy-content mt-3">Dodaj wpis</button></Link>
                        <Link to='/profile'><button className="btn btn-light justivy-content ml-1 mt-3">Profil</button></Link>
                        <button className="btn btn-light justivy-content ml-1 mt-3">Wyloguj się</button>
                    </div>
                </div>
            </>
        )
    }

}

export default HeaderAfterLogin;

