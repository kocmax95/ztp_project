import React from 'react';
import 'bootstrap/dist/css/bootstrap.css'
// import { editProfile } from '../Api/user';
import { getPosts } from '../Api/post'
import {answear} from '../Api/answer'


class Card extends React.Component {
    state = {
        name: "",
        date: "",
        title: "",
        description: "",
        mark: 2,
        comment: "",
        result: []
    }


    componentDidMount = async () => {
        const postsData = await getPosts();
        console.log(postsData);

        this.setState({
            title: postsData.Title,
            description: postsData.Content,
            result: postsData

        })

        //     const accesToken = this.GetAccesToken();
        //     const allPosts = await getPosts(accesToken);
        //     console.log(allPosts)
    }
    // GetAccesToken = () => {
    //     return sessionStorage.getItem('accesToken');
    // }

    handleBtnAdd = () => {
        this.setState({
            mark: this.state.mark + 1
        })
    }
    handleBtnSub = () => {
        this.setState({
            mark: this.state.mark - 1
        })
    }
    handleTextareaAddComent = (e) => {
        this.setState({
            comment: e.target.value
        })
    }
    handleBtnAddComent = async(e) => {
        e.preventDefault();
        console.log(`dodano`);
        // const accesToken = sessionStorage.getItem('accesToken');
        const userId = sessionStorage.getItem('userId');
        const answerData = {
            Content: this.state.comment,
            Author: userId,
            BlogEntry: '5e39f658eebab8120c98c2ef'
        }

        const result = await answear(answerData);
        return result;
    }
    render() {

        const loggedIn = sessionStorage.getItem('userId');
        return (
            <>
                <div className="container">
                    <div className="card border-dark bg-light mt-5">
                        <div className=" card-header d-flex justify-content-between">
                            <h5 className="m-2 p-1">Robert Lewandowski</h5>
                            <h5 className="m-2 p-1">Added: 31.03.2020</h5>
                            <div className="m-2 d-flex justify-content-between">
                                <h3 className="mr-1">{this.state.mark}</h3>
                                {loggedIn === null ? null : <><div className="btn btn-success font-weight-bold mr-1" onClick={this.handleBtnAdd}>+</div>
                                    <div className="btn btn-danger font-weight-bold" onClick={this.handleBtnSub}>-</div></> }

                            </div>
                        </div>
                        <div className="card-body">
                            <div>
                                <h5 className="card-title">Dzień z życia sportowca.</h5>
                                <p className="card-text ml-3">Lorem ipsum dolor, sit amet consectetur adipisicing elit. Iusto mollitia, reiciendis nostrum blanditiis sequi possimus quis eos veniam vero, doloremque, modi explicabo labore nemo autem dolorem voluptatibus ab? Esse, minima.</p>
                            </div>
                            <div className="mt-3 ml-5 ">
                                {/* Do Dodania jednego komentarza */}
                                <div className="mb-2 card border-dark">
                                    <div className="card-header d-flex justify-content-between">
                                        <div>{this.state.name}</div>
                                        <div>Added 15 mins ago</div>
                                    </div>
                                    <div className="card-body">
                                        Chciałbym mieć takie życie. Jakbym tyle zarabiał to mógłbym pracować dwa razy cięzej.. :D
                                    </div>
                                    <div className="card-footer">

                                    </div>
                                </div>
                                {/* Koniec komentarza */}

                                <div className="mb-2 card border-dark">
                                    <div className="card-header d-flex justify-content-between">
                                        <div>Michał Niedzielski</div>
                                        <div>Added 2 mins ago</div>
                                    </div>
                                    <div className="card-body">
                                        Zaimponowałeś mi, też chce tak żyć. Uwielbiam treningi i sport dlatego postanowiłem zmienić swoje życie. Zacząłem chodzić wcześniej spać i więcej ćwiczyć. Zmieniłem również dzietę, od tego czasu jestem mniej senny w ciągu dnia
                                </div>
                                    <div className="card-footer">

                                    </div>
                                </div>
                            </div>
                        </div>
                        {loggedIn === null ? null : <div className="card-footer d-flex ">
                            <div className="pr-3 pt-1 h5">Michał Niedzielski:</div>
                            <textarea type="text" className="rounded  col-6" onChange={this.handleTextareaAddComent} value={this.state.comment} placeholder="Napisz komentarz..." />
                            <button type="submit" className="" onClick={this.handleBtnAddComent}>Dodaj</button>
                        </div>}
                    </div>
                </div>
            </>
        )
    }

}

export default Card;





