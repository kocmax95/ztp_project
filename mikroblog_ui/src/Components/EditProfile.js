import React from 'react';
import {Link} from 'react-router-dom'
import 'bootstrap/dist/css/bootstrap.css'
import { editProfile, getById } from '../Api/user'


class EditProfile extends React.Component {
    constructor(){
        super();
        this.state = {
            name: '',
            surname: '',
            city: '',
            age: '',
    
        }
    }

    componentDidMount = async () =>{
        const userId = this.GetUserId();
        const userData = await getById(userId);
        console.log('userData:',userData);
        
        this.setState({
            name: userData.FirstName,
            surname: userData.LastName,
            city: userData.City,
            age: userData.Age,
        });
        
    }
    handleInputChangeName = (e) => {
        this.setState({
            name: e.target.value,
        })
    }

    handleInputChangeSurname = (e) => {
        this.setState({
          surname: e.target.value,
        })
    }
    handleInputChangeCity = (e) => {
        this.setState({
          city: e.target.value,
        })
    }
    handleInputChangeAge = (e) => {
        this.setState({
          age: e.target.value,
        })
    }

    handleSubmitProfile = async (e) => {
        // e.preventDefault();

        const editProfileData = {
            _id: this.GetUserId(),
            FirstName: this.state.name,
            LastName: this.state.surname,
            City: this.state.city,
            Age: this.state.age
        }

        await editProfile(editProfileData);
        console.log(editProfileData);
    }

    GetUserId = () => {
        return sessionStorage.getItem('userId');
    }

    render() {
        return (
            <>
                <div className="container">
                    <div className="card col-4 mx-auto mt-5">
                        <div className="mt-3 text-center">
                            <h4>Edycja Profilu</h4>
                        </div>
                        <div className="card-body">
                            <form>
                                <div className="form-group">
                                    <label htmlFor="editName">Imię</label>
                                    <input type="text" onChange={this.handleInputChangeName} value={this.state.name} className="form-control" id="editName" />
                                </div>
                                <div className="form-group">
                                    <label htmlFor="editSurname">Nazwisko</label>
                                    <input type="text" onChange={this.handleInputChangeSurname} value={this.state.surname} className="form-control" id="editSurname" />
                                </div>
                                <div className="form-group">
                                    <label htmlFor="editCity">Miejscowość</label>
                                    <input type="text" onChange={this.handleInputChangeCity} value={this.state.city} className="form-control" id="editCity" />
                                </div>
                                <div className="form-group">
                                    <label htmlFor="editAge">Wiek</label>
                                    <input type="text" onChange={this.handleInputChangeAge} value={this.state.age} className="form-control" id="editAge" />
                                </div>
                                <Link to='profile'><button type="submit" onClick={this.handleSubmitProfile} clasName="btn btn-success" >Zapisz zmiany</button></Link>
                            </form>

                        </div>
                    </div>
                </div>
            </>
        )
    }

}

export default EditProfile;





