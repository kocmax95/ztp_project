import React from 'react';
import 'bootstrap/dist/css/bootstrap.css'
import { addPost } from '../Api/post'


class AddPost extends React.Component {
    state = {
        title: "",
        description: ""
    }

    handleInputTitle = (e) => {
        this.setState({
            title: e.target.value
        })
    }
    handleInputDescription = (e) => {
        this.setState({
            description: e.target.value
        })
    }
    handleSubmitBtnAdd = async (e) => {
        e.preventDefault();

        // Pobieram user Id z session Storage
        const userId = this.GetUserId();
        console.log('userId: ', userId);

        const addPostData = {
            Title: this.state.title,
            Content: this.state.description,
            Author: userId
        }
        if(userId === null){
            alert(`Musisz być zalogowany żeby dodać wpis`)
        } else{
            console.log(`wszystko ok wpis został dodany`);
        }

        const result = await addPost(addPostData);
        console.log(result);
        // console.log(userId);


    }

    GetUserId = () => {
        return sessionStorage.getItem('userId');
    }
    render() {
        return (
            <>
                <div className="container">
                    <div className="card mt-5">
                        <div className="card-header font-weight-bold">
                            Dodaj Wpis:
                        </div>
                        <div className="card-body ">
                            {/* <h5>Użytkownik:</h5> */}
                            <form>
                                <div className="form-group col-5">
                                    <label htmlFor="">Tytuł:</label>
                                    <input type="text" className="form-control" onChange={this.handleInputTitle} />
                                </div>
                                <div className="form-group col-12">
                                    <label htmlFor="">Opis:</label>
                                    <input type="text" className="form-control" onChange={this.handleInputDescription} />
                                </div>
                            </form>
                        </div>
                        <div className="card-footer">
                            <button className="btn btn-success col-2" onClick={this.handleSubmitBtnAdd}>Dodaj</button>
                        </div>
                    </div>
                </div>
            </>
        )
    }
}

export default AddPost;





