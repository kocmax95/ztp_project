import React from 'react';
import {Link} from "react-router-dom";

import 'bootstrap/dist/css/bootstrap.css'

class Header extends React.Component {
    state = {}
    render() {
        return (
            <>
                    <div className="container d-flex justify-content-between ">
                        <Link to='/'><h1 className="display-4">Microblog</h1></Link>
                        <div>
                            <Link to='/addPost'><button className="btn btn-light justivy-content mt-3">Dodaj wpis</button></Link>
                            <Link to='/login'><button className="btn btn-light justivy-content ml-1 mt-3">Zaloguj się</button></Link>
                            <Link to='/register'><button className="btn btn-light justivy-content ml-1 mt-3">Zarejestuj się</button></Link>
                        </div>
                    </div>
            </>
        )
    }

}

export default Header;





